CREATE TABLE IF NOT EXISTS plugin_facturation_factures (
	id					INTEGER PRIMARY KEY,
	type_facture		INTEGER NOT NULL DEFAULT 0,
	numero				TEXT NOT NULL UNIQUE,
	receveur_membre 	INTEGER NOT NULL, -- bool
	receveur_id			INTEGER NOT NULL,
	date_emission		TEXT NOT NULL, -- CHECK (date(date_emission) IS NOT NULL AND date(date_emission) = date_emission),
	date_echeance		TEXT NOT NULL, -- CHECK (date(date_echeance) IS NOT NULL AND date(date_echeance) = date_echeance),
	reglee				INTEGER DEFAULT 0, -- bool
	archivee			INTEGER DEFAULT 0, -- bool
	moyen_paiement		TEXT NOT NULL,
	contenu 			TEXT NOT NULL,
	total   			INTEGER DEFAULT 0

	-- FOREIGN KEY(moyen_paiement) REFERENCES compta_moyens_paiement(code)
);

CREATE TABLE IF NOT EXISTS plugin_facturation_clients (
	id					INTEGER PRIMARY KEY,
	nom					TEXT NOT NULL,
	adresse				TEXT NOT NULL,
	code_postal			TEXT NOT NULL,
	ville				TEXT NOT NULL,
	-- date_creation		INTEGER NOT NULL,
	date_creation		TEXT NOT NULL DEFAULT CURRENT_DATE CHECK (date(date_creation) IS NOT NULL AND date(date_creation) = date_creation), -- Date d\'inscription
	telephone			TEXT,
	email				TEXT
);


CREATE TABLE IF NOT EXISTS plugin_facturation_paiement
-- Moyens de paiement
(
    code TEXT NOT NULL PRIMARY KEY,
    nom TEXT NOT NULL
);

--INSERT INTO compta_moyens_paiement (code, nom) VALUES ('AU', 'Autre');
INSERT OR IGNORE INTO plugin_facturation_paiement (code, nom) VALUES ('CB', 'Carte bleue');
INSERT OR IGNORE INTO plugin_facturation_paiement (code, nom) VALUES ('CH', 'Chèque');
INSERT OR IGNORE INTO plugin_facturation_paiement (code, nom) VALUES ('ES', 'Espèces');
INSERT OR IGNORE INTO plugin_facturation_paiement (code, nom) VALUES ('PR', 'Prélèvement');
INSERT OR IGNORE INTO plugin_facturation_paiement (code, nom) VALUES ('TI', 'TIP');
INSERT OR IGNORE INTO plugin_facturation_paiement (code, nom) VALUES ('VI', 'Virement');
INSERT OR IGNORE INTO plugin_facturation_paiement (code, nom) VALUES ('HA', 'HelloAsso');
INSERT OR IGNORE INTO plugin_facturation_paiement (code, nom) VALUES ('AU', 'Autre');

-- Modif DD -- ajout de la table des textes associés aux CERFA
CREATE TABLE IF NOT EXISTS plugin_facturation_txt_cerfa
-- Textes explicatifs associés aux CERFA
(
    id PRIMARY KEY,
    menu TEXT NOT NULL UNIQUE,
    texte TEXT NOT NULL
);

----
-- Data dump for plugin_facturation_txt_cerfa, a total of 4 rows
----
INSERT OR IGNORE INTO "plugin_facturation_txt_cerfa" ("id","menu","texte") VALUES ('0','Aucun','');
INSERT OR IGNORE INTO "plugin_facturation_txt_cerfa" ("id","menu","texte") VALUES ('1','HelloAsso','Don via HelloAsso');
INSERT OR IGNORE INTO "plugin_facturation_txt_cerfa" ("id","menu","texte") VALUES ('2','Frais de déplacement','Renonciation aux remboursements de frais de déplacement');
INSERT OR IGNORE INTO "plugin_facturation_txt_cerfa" ("id","menu","texte") VALUES ('3','Don en nature','Don en nature');

-- CREATE TABLE IF NOT EXISTS plugin_facturation_produits (
-- 	id					INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,
-- 	designation	TEXT,
-- 	valeur_prix	REAL
-- );
