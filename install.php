<?php
namespace Paheko;
use Paheko\Entities\Files\File;
use Paheko\Plugin\Facturation\Facture;

$db = DB::getInstance();

$db->import(dirname(__FILE__) . "/data/schema.sql");

$plugin->registerSignal('menu.item', [Facture::class, 'menuItem']);