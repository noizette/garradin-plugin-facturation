{include file="_head.tpl" title="Modifier un document — %s"|args:$plugin.name current="plugin_%s"|args:$plugin.id js=1}
{include file="%s/templates/_menu.tpl"|args:$plugin_root current="index"}

{include file="%s/templates/_form.tpl"|args:$plugin_root}

{include file="_foot.tpl"}
